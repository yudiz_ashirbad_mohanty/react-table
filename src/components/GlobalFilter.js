export const GlobalFilter = ({ filter, setFilter }) => {

  return (
    <span style={{display:'flex',margin:'20px',justifyContent:'center'}}>
      Search:{' '}
      <input style={{padding:'0px 80px',borderRadius:'2px solid'}} value={filter || ''}
      onChange={e => setFilter(e.target.value)}
      />
    </span>
  )
}