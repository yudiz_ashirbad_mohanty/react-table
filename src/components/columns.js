

export const COLUMNS = [
  {
    Header: 'Id',
  
    accessor: 'id',
    disableFilters: true,
    sticky: 'left'
  },
  {
    Header: 'Name',
   
    accessor: 'Name',
    sticky: 'left'
  },
  {
    Header: 'Type',
 
    accessor: 'Type',
    sticky: 'left'
  },
  {
    Header: 'Language',
  
    accessor: 'Language',
   
  },
  {
    Header: 'Genre(s)',
    accessor: 'Genre(s)'
  },
  {
    Header: 'Runtime',
   
    accessor: 'Runtime'
  },

  {
    Header: 'Status',
   
    accessor: 'Status'
  },
]

export const GROUPED_COLUMNS = [
  {
    Header: 'Id',
    accessor: 'id'
  },
  {
    Header: 'Tv show',

    columns: [
      {
        Header: 'Name',
       
        accessor: 'Name'
      },
      {
        Header: 'Type',
       
        accessor: 'Type'
      }
    ]
  },
  {
    Header: 'Details',

    columns: [
      {
        Header: 'Language',
       
        accessor: 'Language'
      },
      {
        Header: 'Genre(s)',
        
        accessor: 'Genre(s)'
      },
      {
        Header: 'Runtime',
      
        accessor: 'Runtime'
      },
      {
        Header: 'Status',
        accessor: 'Status'
      }
    ]
  }
]