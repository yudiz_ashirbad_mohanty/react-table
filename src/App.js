import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";


import { Helmet } from "react-helmet";
import Table from "./components/Table";
import NavBar from "./components/NavBar";

function App() {
  return (
    <>
      <Router>
        <Helmet>
          <title>TV SHOWS</title>
          <meta name="description" content=""/>
        </Helmet>
    <NavBar/>
        <div className="pages">
          <Switch>
            <Route path="/" component={Table} exact />
          </Switch>
        </div>
      </Router>
    </>
  );
}

export default App;